import random
import turtle

turtle.bgcolor("slategray")
choix = ["eren", "bertolto", "reiner", "durel", "bonnard", "bezain"]
solution = random.choice(choix)

tentatives = 0
affichage = ""
lettres_trouvees = ""

for l in solution:
    affichage = affichage + "_ "

turtle.up()
turtle.goto(0, 220)
turtle.write(">> Bienvenue dans le jeu du pendu <<", move=False,
             align="center", font=("Arial", 25, "normal"))
turtle.goto(0, 200)
turtle.write("Par Marianne Bezain et Mathilde Bonnard", move=False,
             align="center", font=("Arial", 15, "normal"))
turtle.goto(-50,0)

while tentatives <= 10:
    T1 = turtle.Turtle()
    T1.clear()
    T1.up()
    T1.goto(-200,0)
    T1.write(affichage, move=False, align="center",
                         font=("Arial", 20, "normal"))
    T1.goto(-500,0)

    proposition = turtle.textinput("Pendu", "proposez une lettre : ")[0:1].lower()

    turtle.down()

    if proposition in solution:
        lettres_trouvees = lettres_trouvees + proposition
        print("-> Bien vu!\n")
    else:
        tentatives = tentatives + 1
        print("-> Non\n")

        if tentatives == -1:
            print("Il vous reste : 0 tentatives")
        else:
            print("Il vous reste : ", 11 - tentatives, " tentatives")

        if tentatives == 1:
            turtle.color("darksalmon")
            turtle.forward(100)
            turtle.backward(50)

        elif tentatives == 2:
            turtle.color("darksalmon")
            turtle.left(90)
            turtle.forward(125)
            turtle.right(90)

        elif tentatives == 3:
            turtle.color("darksalmon")
            turtle.forward(70)
            turtle.right(90)

        elif tentatives == 4:
            turtle.color("darksalmon")
            turtle.forward(20)
            turtle.up()
            turtle.right(90)

        elif tentatives == 5:
            turtle.color("palevioletred")
            turtle.forward(1)
            turtle.down()
            turtle.width(2)
            turtle.circle(10)

        elif tentatives == 6:
            turtle.color("crimson")
            turtle.up()
            turtle.left(90)
            turtle.forward(20)
            turtle.down()
            turtle.forward(30)

        elif tentatives == 7:
            turtle.color("crimson")
            turtle.backward(23)
            turtle.right(30)
            turtle.forward(20)
            turtle.backward(20)
            turtle.left(60)

        elif tentatives == 8:
            turtle.color("crimson")
            turtle.forward(20)
            turtle.backward(20)
            turtle.right(30)

        elif tentatives == 9:
            turtle.color("crimson")
            turtle.forward(23)
            turtle.left(26)
            turtle.forward(24)
            turtle.backward(24)

        elif tentatives == 10:
            turtle.color("crimson")
            turtle.right(52)
            turtle.forward(24)
            turtle.up()
            turtle.forward(80)

    affichage = ""
    for x in solution:
        if x in lettres_trouvees:
            affichage += x + " "
        else:
            affichage += "_ "

    if "_" not in affichage:
        print(">>> Gagné! <<<")
        break

print("\n    * Fin de la partie *    ")
